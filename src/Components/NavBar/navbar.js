import React from 'react'
import NavBarItem from './navbar-item'

export default function NavBar(){
    return (
        <nav className="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
            <div className="container">
                <div className="navbar-translate">
                    <a className="navbar-brand" href="/">Nome da Loja</a>
                </div>
                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav ml-auto">
                    <NavBarItem href="/" label="Sobre"/>
                    <NavBarItem href="https://www.google.com" label="Catalogo"/>
                    <NavBarItem href="https://www.google.com" label="Contato"/>
                    <NavBarItem href="/signup" label="Entrar"/> 
                    <NavBarItem href="/CadastraProduto" label="Cadastra Produto"/> 
                    </ul>
                </div>
            </div>
        </nav>
    )
}