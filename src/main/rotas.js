import React, { Component } from 'react'

import CadastraProduto from '../views/CadastroPage/cadastraProduto'
import SignUp from '../views/SignUpPage/signUp'
import Home from '../views/HomePage/home'
import SignIn from '../views/SignInPage/SignIn2'

import {BrowserRouter,Route,Redirect,Switch} from 'react-router-dom'
import { isAuthenticated } from '../app/service/auth'



const PrivateRoute = ({component:Component, ...rest}) => (
<Route
{...rest}
    render={props =>
        isAuthenticated() ? (
            <Component {...props}/>
        ) : (
            <Redirect to={{pathname :"/",state:{from : props.location} }} />
        )
    }
    />
);

function Rotas(){
    return(
        <BrowserRouter>
            <Switch>                
                <Route path="/signup" component={SignUp} />
                <Route path="/CadastraProduto" component={CadastraProduto}/>
                <Route path="/signIn" component={SignIn}/>
                <Route path="/" component={Home} /> 
                <PrivateRoute path="/app" component={() => <h1>App</h1>} />
                <Route path="*" component={() => <h1>Page not found</h1>} />
            </Switch>
        </BrowserRouter>
    )
}
export default Rotas;