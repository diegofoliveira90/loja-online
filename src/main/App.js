import React from 'react';
import FooterPage from '../Components/Footer/footerPage'
import Navbar from '../Components/NavBar/navbar'
import Rotas from './rotas'
import './app.css'

class App extends React.Component {
  render() {
    return (
      <div className="conteudo">
        <Rotas />
        <FooterPage />
      </div>  
    )
  }
}

export default App;
