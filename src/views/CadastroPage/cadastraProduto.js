import React from 'react'
import FormGroup from '../../Components/FormGroup/formGroup'

import api from '../../app/api'
import { isAuthenticated } from '../../app/service/auth'
import { Form } from "../../styles/styleCadastraProduto";
class CadastraProduto extends React.Component{
    state={
        nome:'',
        detalhes:'',
        largura:'',
        altura:'',
        profundidade:'',
        preco:'',
        error:''
    }
    handleSignIn = async e => {
        e.preventDefault();
        if(isAuthenticated){
            const { nome, detalhes,largura,altura,profundidade,preco } = this.state;
            if (!nome || !detalhes || !largura || !altura || !profundidade || !preco) {
              this.setState({ error: "Preencha todos os campos!" });
            } else {
              try {                
                const response = await api.post("/produto", { nome, detalhes,largura,altura,profundidade,preco });           
                this.props.history.push("/app");
              } catch (err) {
                  console.log(err)
                this.setState({
                  error:
                    "Houve um problema com o cadastro!!"
                });
              }
            }
        }else{
            this.props.history.puss("/signIn")
        }
      };
    render(){
        return(
            
                <div className='row'>
                <Form onSubmit={this.handleSignIn}>
                {this.state.error && <p>{this.state.error}</p>}
                    <div className='col-lg-6'>
                        <div className='bs-component'>
                            <FormGroup label='Nome' htmlfor='inputName'>
                                <input type="text" value={this.state.nome} onChange={e=> this.setState({nome:e.target.value})} className="form-control" placeholder="Nome do produto" id="inputName"/>
                            </FormGroup>
                            
                            <FormGroup label='Detalhes' htmlfor='inputDetalhes'>
                                <input type="text" value={this.state.detalhes} onChange={e=> this.setState({detalhes:e.target.value})} className="form-control" placeholder="Detalhes do produto" id="inputDetalhes"/>
                            </FormGroup>
                             
                            <FormGroup label='Largura' htmlfor='inputLargura'>    
                                <input type="text" value={this.state.largura} onChange={e=> this.setState({largura:e.target.value})} className="form-control" placeholder="Largura maxima do produto" id="inputLargura"/>
                            </FormGroup>
                             <FormGroup label='Altura' htmlfor='inputAltura'>                               
                                <input type="text" value={this.state.altura} onChange={e=> this.setState({altura:e.target.value})} className="form-control" placeholder="Altura maxima do produto" id="inputAltura"/>
                            </FormGroup>
                            <FormGroup label='Profundidade' htmlfor='inputProfundidade'>                              
                                <input type="text" value={this.state.profundidade} onChange={e=> this.setState({profundidade:e.target.value})} className="form-control" placeholder="Distancia maxima da parede" id="inputProfundidade"/>
                            </FormGroup>
                            <FormGroup label='Preço' htmlfor='inputPreco'>                           
                            <div className="form-group">
                                <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">R$</span>
                                </div>
                                <input type="text" value={this.state.preco} onChange={e=> this.setState({preco:e.target.value})} className="form-control" aria-label="Amount (to the nearest dollar)"/>
                                <div className="input-group-append">
                                    <span className="input-group-text">.00</span>
                                </div>
                                </div>
                            </div>
                            </FormGroup>       
                            <FormGroup label='Imagem' htmlfor='inputImagem'>
                            </FormGroup>
                            <button type="submit" className="btn btn-primary">Salvar</button>
                            <button type="button" className="btn btn-danger">Cancelar</button>
                        </div>
                    </div>
                    </Form>
                </div>            
        )
    }
}
export default CadastraProduto