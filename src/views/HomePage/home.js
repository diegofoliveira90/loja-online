import React from 'react'
import Header from '../../Components/Header/header'
import Carrosel from '../../Components/Carousel/carrosel'
import Post from '../../Components/Post/Post'
import Grid from '@material-ui/core/Grid';
import Api from '../../app/api'

class Home extends React.Component{
    /*
        CRIAR UM METODO GET PARA BUSCAR NA API TODAS AS CATEGORIAS QUE EXISTEM PARA LISTAR NA CONST SECTIONS
    */
   
    state = {
        sections : [],
        featuredPosts : [],
    }
    render(){

        Api.get('http://localhost:8080/categoria/').then(res =>{
            this.setState({
                sections:res.data
            })
        })

        const sections = [
            { title: 'Technology', url: '#' },
            { title: 'Design', url: '#' },
            { title: 'Culture', url: '#' },
            { title: 'Business', url: '#' },
            { title: 'Politics', url: '#' },
            { title: 'Opinion', url: '#' },
            { title: 'Science', url: '#' },
            { title: 'Health', url: '#' },
            { title: 'Style', url: '#' },
            { title: 'Travel', url: '#' },
          ];
          const featuredPosts = [
            {
              title: 'Featured post1',
              date: 'Nov 12',
              description:
                'This is a wider card with supporting text below as a natural lead-in to additional content.',
              image: 'https://source.unsplash.com/random',
              imageText: 'Image Text',
            },
            {
              title: 'Post title2',
              date: 'Nov 11',
              description:
                'This is a wider card with supporting text below as a natural lead-in to additional content.',
              image: 'https://source.unsplash.com/random',
              imageText: 'Image Text',
            },
            {
                title: 'Featured post3',
                date: 'Nov 12',
                description:
                  'This is a wider card with supporting text below as a natural lead-in to additional content.',
                image: 'https://source.unsplash.com/random',
                imageText: 'Image Text',
              }
              ,
            {
                title: 'Featured post4',
                date: 'Nov 12',
                description:
                  'This is a wider card with supporting text below as a natural lead-in to additional content.',
                image: 'https://source.unsplash.com/random',
                imageText: 'Image Text',
              }
              ,
            {
                title: 'Featured post5',
                date: 'Nov 12',
                description:
                  'This is a wider card with supporting text below as a natural lead-in to additional content.',
                image: 'https://source.unsplash.com/random',
                imageText: 'Image Text',
              }
              ,
            {
                title: 'Featured post6',
                date: 'Nov 12',
                description:
                  'This is a wider card with supporting text below as a natural lead-in to additional content.',
                image: 'https://source.unsplash.com/random',
                imageText: 'Image Text',
              }
          ];

        return(
            <>
                <Header title="Loja" sections={sections} />
                <Carrosel></Carrosel>
                <Grid container spacing={0}>
                    {featuredPosts.map(post => (
                    <Post key={post.title} post={post} />
                    ))}
                </Grid>          
            </>
        )
    }
}
export default Home;