import axios from 'axios'
import { getToken } from './service/auth'

const httpClient = axios.create({
    baseURL:'http://localhost:8080'
})

httpClient.interceptors.request.use(async config => {
    const token = getToken();
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  });


class apiService{

    constructor(apiUrl){
        this.apiUrl = apiUrl;
    }

    post(url,objeto){
        console.log(url)
        const requestUrl = `${this.apiUrl}${url}`        
        return httpClient.post(requestUrl,objeto)
    }
    put(url,objeto){
        const requestUrl = `${this.apiUrl}${url}`
        return httpClient.post(requestUrl,objeto)
    }
    delete(url,objeto){
        const requestUrl = `${this.apiUrl}${url}`
        return httpClient.post(requestUrl,objeto)
    }
    get(url,objeto){
        const requestUrl = `${this.apiUrl}${url}`
        return httpClient.post(requestUrl,objeto)
    }

}export default apiService